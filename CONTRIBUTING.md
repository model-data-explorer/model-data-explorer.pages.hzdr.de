# Contributing to the documentation of the Model Data Explorer

First of all, thanks! :tada: We are always happy about any kind of
contributions to help us improving the model data explorer and it's
documentation.

The source Model Data Explorer is publicly available at the Gitlab of the
_Helmholtz-Zentrum Dresden Rossendorf_, HZDR. You can sign up at
https://gitlab.hzdr.de with your Github account or through the Helmholtz AAI
(if you are employed at a member insitution of the HGF).

## Report issues, give feedback or ask for support

If you encounter any issues or have questions for support, please create an
issue in the main repository of this documentation, here: https://gitlab.hzdr.de/model-data-explorer/model-data-explorer.pages.hzdr.de/-/issues

If you want to reach the core development team directly, please do not hesitate
to send a mail to hcdc_support@hereon.de

## Contribute to the documentation

If you want to build and modify the documentation yourself, please have a look
into the _Installation_ section in the [README](README.md) of this repository.
