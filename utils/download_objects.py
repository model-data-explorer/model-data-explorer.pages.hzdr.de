"""Utitlity script to download the objects.inv file from gitlab."""

import os
import requests

TOKEN = os.getenv("CI_API_TOKEN")

for project, project_id in [
    ("prototype", 3408),
]:
    print(f"Downloading objects.inv for project: {project}")
    uri = (
        f"https://gitlab.hzdr.de/api/v4/projects/{project_id}/jobs"
        "?scope[]=success"
    )
    response = requests.get(uri, headers={"PRIVATE-TOKEN": TOKEN})
    assert response.status_code == 200, response
    jobs = response.json()
    latest = jobs[0]
    job_id = latest["id"]

    uri = (
        f"https://gitlab.hzdr.de/api/v4/projects/{project_id}/jobs/{job_id}/"
        "artifacts/public/objects.inv"
    )

    response = requests.get(uri, headers={"PRIVATE-TOKEN": TOKEN})
    assert response.status_code == 200, response

    with open(f"source/{project}.objects.inv", "wb") as f:
        for chunk in response.iter_content():
            if chunk:
                f.write(chunk)

print("Success.")
