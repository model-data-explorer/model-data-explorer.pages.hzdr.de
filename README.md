# Welcome to the Model Data Explorer

This is the landing page of the model data explorer documentation. This
document is split up into several projects:

1. The main documentation (here)
2. The prototype documentation (https://gitlab.hzdr.de/model-data-explorer/prototype)

Please view this documentation online at https://model-data-explorer.readthedocs.io/

## Installation

If you want to build and modify this documentation locally, you need to install
sphinx. The [.gitlab-ci.yml](.gitlab-ci.yml) file is always a good starting
point as it shows you how we build it as part of the continuous integration on
gitlab.

Anyway, here are the steps that you need to build this documentation.

1. Install python on your computer, from source or via [miniconda](https://conda.io/en/latest/miniconda.html)
2. clone this repository via `git clone https://gitlab.hzdr.de/model-data-explorer/model-data-explorer.pages.hzdr.de.git`
3. `cd` into the directory that you cloned and run `pip install -r requirements.txt`
4. You now need to download the `objects.inv` file from https://model-data-explorer.pages.hzdr.de/prototype/objects.inv and save it as `source/prototype.objects.inv` (unfortunately the pages on gitlab are not yet publicly available)
5. make your changes in the `*.rst` files, e.g. in [`index.rst`](source/index.rst)
6. run `make html` on linux or `make.bat html` on Windows to generate the
documentation.
7. open `build/html/index.html` in your local browser.

This documentation is written in restructured Text with [Sphinx](https://www.sphinx-doc.org/en/master/). If you want to learn more about it, please have a look at the
examples in the [Sphinx documentation](https://www.sphinx-doc.org/en/master/).

Note: We highly recommend to use Visual Studio Code with the
[reStructuredText extension](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext) to enable a live preview of the
documentation while writing.

## Contributing

Please see the [`CONTRIBUTING.md`](CONTRIBUTING.md) file about how to edit and
contribute to this documentation.

## Disclaimer

This documentation is published under a CC-BY 4.0 license. Please see the
*LICENSE* file in the source code repository for details.

Copyright (c) 2021, Helmholtz-Zentrum hereon GmbH
